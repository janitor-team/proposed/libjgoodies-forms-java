#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
TAR=../libjgoodies-forms-java_$2.orig.tar.gz
DIR=libjgoodies-forms-java-$2.orig

# clean up the upstream tarball
unzip $3
mv forms-* $DIR
tar -c -z -f $TAR --exclude '*/docs/api*' --exclude '*.jar' $DIR
rm -rf $DIR $3

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
